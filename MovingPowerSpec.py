import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation

#input signal
x=np.linspace(0,15000-1,15000)
y=np.sin(2*np.pi*x/20)

x1=np.linspace(5000,15000-1,10000)
x1=x1.astype(np.int)
y1=np.sin(2*np.pi*x1/150)
y[x1]=y[x1]+y1

x2=np.linspace(10000,15000-1,5000)
x2=x2.astype(np.int)
y2=np.sin(2*np.pi*x2/70)
y[x2]=y[x2]+y2

#create index for generating windows
window_size=1000
step_size=50
sample_len=len(x)
idx_init=np.arange(0,sample_len-window_size,step_size)
idx_end=np.arange(window_size,sample_len,step_size)


#chopping input signal into windows
windows=np.zeros([window_size,len(idx_end),2]) #create array with size: window size-by-# of windows
windows_x=np.arange(0,window_size)
for i in range(0,len(idx_end)):
    windows[:,i,1]=y[idx_init[i]:idx_end[i]]
    windows[:,i,0]=x[idx_init[i]:idx_end[i]]

#Create initial window
fig,ax = plt.subplots(3,1)
ax[0].plot(x,y)
ax[0].set_title('whole window')
ax[1].set_xlim([0,window_size])
ax[1].set_ylim([-3,3])
ax[1].set_title('moving window')
ax[2].set_title('power spectrum')
ax[2].set_ylim([-1,500])
ax[2].set_xlim([0,0.2])

line1,=ax[0].plot([],[],lw=5)
line2,=ax[1].plot([],[],lw=2)
line3,=ax[2].plot([],[],lw=2)
line=[line1,line2,line3]

# Animate using code from Jake Vanderplas http://jakevdp.github.com
# First set up the figure, the axis, and the plot element we want to animate


# initialization function: plot the background of each frame
def init():
    line[0].set_data([], [])
    return line[0],

# animation function.  This is called sequentially
def animate(i):
    # line[0].set_data
    line[0].set_data(windows[:,i,0],np.zeros(window_size))
    line[1].set_data(windows_x, windows[:,i,1])

    #fft
    freq=np.fft.fftfreq(windows.shape[0],1)#sampling freq is 1
    ps=np.abs(np.fft.fft(windows[:,i,1]))
    line[2].set_data(freq,ps)
    return line,

# call the animator.  blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig, animate, init_func=init,frames=len(idx_end))
#                                frames=200, interval=20, blit=True)
# anim = animation.FuncAnimation(fig, animate, init_func=init,
#                                frames=200, interval=20, blit=True)

# save the animation as an mp4.  This requires ffmpeg or mencoder to be
# installed.  The extra_args ensure that the x264 codec is used, so that
# the video can be embedded in html5.  You may need to adjust this for
# your system: for more information, see
# http://matplotlib.sourceforge.net/api/animation_api.html
print("Saving...")
anim.save('basic_animation.mp4', fps=10, extra_args=['-vcodec', 'libx264'])
print("Save complete")
plt.show()